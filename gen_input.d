import core.sys.windows.winnt : FILE_ATTRIBUTE_READONLY;

import std.algorithm;
import std.conv;
import std.file;
import std.regex;
import std.stdio;
import std.string;

void main()
{
/*
	auto dvorakMap = [
//		"IK_-"         : "IK_",
//		"IK_="         : "IK_",
		"IK_Q"         : "IK_SingleQuote",
		"IK_W"         : "IK_Comma",
		"IK_E"         : "IK_Period",
		"IK_R"         : "IK_P",
		"IK_T"         : "IK_Y",
		"IK_Y"         : "IK_F",
		"IK_U"         : "IK_G",
		"IK_I"         : "IK_C",
		"IK_O"         : "IK_R",
		"IK_P"         : "IK_L",
//		"IK_["         : "IK_-",
//		"IK_]"         : "IK_=",
		"IK_A"         : "IK_A",
		"IK_S"         : "IK_O",
		"IK_D"         : "IK_E",
		"IK_F"         : "IK_U",
		"IK_G"         : "IK_I",
		"IK_H"         : "IK_D",
		"IK_J"         : "IK_H",
		"IK_K"         : "IK_T",
		"IK_L"         : "IK_N",
		"IK_Semicolon" : "IK_S",
		"IK_Z"         : "IK_Semicolon",
		"IK_X"         : "IK_Q",
		"IK_C"         : "IK_J",
		"IK_V"         : "IK_K",
		"IK_B"         : "IK_X",
		"IK_N"         : "IK_B",
		"IK_M"         : "IK_M",
		"IK_Comma"     : "IK_W",
		"IK_Period"    : "IK_V",
		"IK_Slash"     : "IK_Z",
	];
*/

	auto keyMap = [
		"IK_F1"               : "IK_F6"               , // Make room for DrinkPotion
		"IK_Home"             : "IK_PageUp"           , // Make room for sword keys
		"IK_1"                : "IK_Home"             , // Draw steel sword
		"IK_2"                : "IK_End"              , // Draw silver sword
		"IK_LControl"         : "IK_Slash"            , // Walk toggle
		"IK_Alt"              : "IK_LControl"         , // Dodge
		"IK_MiddleMouse"      : "IK_R"                , // Throw item
		"IK_Tab"              : "IK_MiddleMouse"      , // Radial menu
		"IK_M"                : "IK_Tab"              , // Map
		"IK_Backspace"        : "IK_None"             , // "Fast menu"
		"IK_N"                : "IK_Backspace"        , // Meditation
		"IK_B"                : "IK_N"                , // Bestiary
		"IK_J"                : "IK_B"                , // Journal
		"IK_C"                : "IK_PageDown"         , // Sheathe / dive
		"IK_K"                : "IK_C"                , // Character
		"IK_V"                : "IK_Backslash"        , // Track quest
		"IK_I"                : "IK_V"                , // Inventory
		"IK_L"                : "IK_P"                , // [L] Alchemy
		"IK_G"                : "IK_U"                , // [G] Glossary
		"IK_O"                : "IK_I"                , // [C] Crafting
	];

	auto lines = readText("input.defaults").splitLines;
	foreach (ref line; lines)
	{
		auto parts = line.findSplit("=");
		if (parts[1].length)
		{
			auto key = parts[0];
			auto action = parts[2];

			// Ignore gamepad buttons
			if (key.startsWith("IK_Pad_"))
				continue;

		//	if (auto pDKey = key in dvorakMap)
		//		key = *pDKey;

			if (auto pDKey = key in keyMap)
				key = *pDKey;

			if (key == "IK_RightMouse")
			{
				if (action == "(Action=LockAndGuard)") // Block
					key = "IK_E"; // merge with "Use"
				else
				if (action == "(Action=Focus,Reprocess)") // Witcher senses
					key = "IK_Alt";
			}

			if (action.canFind("AttackWithAlternateHeavy")) // Heavy attack
				key = "IK_RightMouse";

			if (auto m = action.matchFirst(regex(`^\(Action=DrinkPotion(\d)\)$`))) // Item quick-use
				key = "IK_F" ~ m[1]; // -> F1 .. F4

			if (auto m = action.matchFirst(regex(`^\(Action=Select(Aard|Quen|Yrden|Igni|Axii)\)$`))) // Signs
			{
				key = "IK_" ~ text(1 + ["Aard", "Quen", "Yrden", "Igni", "Axii"].countUntil(m[1])); // -> 1 .. 5
				// Instantly cast signs when pressing the number key
				action ~= "\r\n" ~ key ~ "=(Action=CastSign,State=Duration,IdleTime=0.001)";
				action ~= "\r\n" ~ key ~ "=(Action=CastSignHold,State=Duration,IdleTime=0.2)";
			}

			line = key ~ '=' ~ action;
		}
	}

	enum outFn = "input.settings";
	setAttributes(outFn, getAttributes(outFn) & ~FILE_ATTRIBUTE_READONLY);
	lines.map!(l => l ~ "\r\n").join.toFile(outFn);
	setAttributes(outFn, getAttributes(outFn) |  FILE_ATTRIBUTE_READONLY);
}


/*

Keys:

IK_None

IK_Escape

IK_F1

IK_Backslash
IK_Tilde

IK_1
IK_2
IK_3
IK_4
IK_5
IK_6
IK_7

IK_Tab
IK_Slash
IK_SingleQuote
IK_Comma
IK_Period

IK_A
IK_B
IK_C
IK_D
IK_E
IK_F
IK_G
IK_H
IK_I
IK_J
IK_K
IK_L
IK_M
IK_N
IK_O
IK_P
IK_Q
IK_R
IK_S
IK_T
IK_V
IK_W
IK_X
IK_Y
IK_Z

IK_Alt
IK_LShift
IK_LControl

IK_Semicolon

IK_Home
IK_End
IK_PageUp
IK_PageDown

IK_Space
IK_Enter
IK_Backspace
IK_Delete
IK_Insert

IK_NumPad1
IK_NumPad2
IK_NumPad4

IK_LeftMouse
IK_MiddleMouse
IK_RightMouse

IK_MouseX
IK_MouseY
IK_MouseZ

*/